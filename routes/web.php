<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('index');
});

Route::get('/home', function () {
    return view('home');
});


Route::get('/blog', function () {
    return view('blog');
});


Route::get('/contact-us', function () {
    return view('contact-us');
});


Route::get('/brands', function () {
    return view('brands');
});


Route::get('/red-brand', function () {
    return view('red-brand');
});


Route::get('/our-story', function () {
    return view('our-story');
});


Route::get('/contact-us-section', function () {
    return view('contact-us-section');
})->name('contact');


 Route::get('/footer', function () {
     return view('footer');
 });

 Route::post('/store','FirstController@store');


Route::get('/contactUs2', function () {
    return view('contactUs');
});




