<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $table = "contacts";

    protected $fillable = ['name','phone','mail','created_at','updated_at']; 

    protected $hidden = ['created_at' , 'updated_at'];
}
