<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Contact;

use Illuminate\Support\Facades\Validator;


class FirstController extends Controller
{
    //
    /*public function __construct(){

    }*/

    public function store(Request $request){


    	//$rules = $this->getRules();

    	//$messages = $this->getMessages();
    	$rules = [

    			'name' 	=>'required|max:100',
    			'phone' =>'required|numeric',
    			'mail' 	=>'required|email|unique:contacts,mail'
    		];

    	$messages = [
    			'name.required'                        => 'الاسم مطلوب',
				'phone.required'                       => 'رقم الموبيل مطلوب',
				'mail.required'                        => 'الميل مطلوب ',
				'phone.numeric'                        => ' الموبيل يجب ان يكون رقم',
				'mail.email'                           => 'يجب ان يكون ميل ',
				'mail.unique'                          => ' الميل موجود',

			];

    	$validator = Validator::make($request->all(),$rules,$messages);
    				
    		

    	if($validator->fails()){

    		//return redirect()->back()->withErrors($validator)->withInputs($request->all());
    		//return $validator->errors();
    		//return json_encode($validator ->errors(), JSON_UNESCAPED_UNICODE);
    		return redirect()->to('/contactUs2')->withErrors($validator)->withInputs($request->all());
    	}

    	Contact::create([

    		'name' 	=> $request->name,
    		'phone' => $request->phone,
    		'mail' 	=> $request->mail,
    	]);

    	return redirect('/contactUs2')->with(['success' => 'تم الإضافة بنجاح']);
    		

    }
}


