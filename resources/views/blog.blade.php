<section class="sec hero-section d-block">
    <div class="hero-header text-center mt-5 px-3">
        <h1>Delicious recipes</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore</p>
    </div>
    <img class="hero-bg" src="{{ url('') }}/img/nature2.jpg" alt="">
    <span class="side-menu-icon" style="font-size:30px;cursor:pointer" onclick="openNav()">
        <i class="fas fa-stream"></i>
    </span>
</section>
<section class="sec cd-section row p-5">
    <div class="tea-block d-flex flex-column col-12 col-md-4">
        <img src="{{ url('') }}/img/tea7.jpg" alt="">
        <h3>black tea</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore</p>
    </div>
    <div class="tea-block d-flex flex-column col-12 col-md-4">
        <img src="{{ url('') }}/img/tea6.jpg" alt="">
        <h3>morning tea</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore</p>
    </div>
    <div class="tea-block d-flex flex-column col-12 col-md-4">
        <img src="{{ url('') }}/img/tea2.jpg" alt="">
        <h3>lunch tea</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore</p>
    </div>
</section>

<section class="sec cd-section row p-5">
    <div class="tea-block horizontal row pb-5">
        <img class="col-md-6" src="{{ url('') }}/img/tea9.jpg" alt="">
        <div class="col-md-4 position-relative">
            <h3>black tea</h3>
            <p class="pr-5 pr-md-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore</p>
            <div class="col-md-1 navigation-arrow d-flex d-md-none"><i class="fas fa-chevron-right arrow"></i></div>
        </div>
        <div class="col-md-1 navigation-arrow d-none d-md-flex"><i class="fas fa-chevron-right arrow"></i></div>
    </div>

    <div class="tea-block horizontal row pb-5">
        <img class="col-md-6" src="{{ url('') }}/img/tea1.jpeg" alt="">
        <div class="col-md-4 position-relative">
            <h3>black tea</h3>
            <p class="pr-5 pr-md-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore</p>
            <div class="col-md-1 navigation-arrow d-flex d-md-none"><i class="fas fa-chevron-right arrow"></i></div>
        </div>
        <div class="col-md-1 navigation-arrow d-none d-md-flex"><i class="fas fa-chevron-right arrow"></i></div>
    </div>

    <div class="tea-block horizontal row pb-5">
        <img class="col-md-6" src="{{ url('') }}/img/tea8.jpg" alt="">
        <div class="col-md-4 position-relative">
            <h3>black tea</h3>
            <p class="pr-5 pr-md-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore</p>
            <div class="col-md-1 navigation-arrow d-flex d-md-none"><i class="fas fa-chevron-right arrow"></i></div>
        </div>
        <div class="col-md-1 navigation-arrow d-none d-md-flex"><i class="fas fa-chevron-right arrow"></i></div>
    </div>

    <button class="load-more">load more</button>
</section>

<link rel="stylesheet" href="{{ url('') }}/css/blog.css">
<script>
    $(function () {
        $.scrollify({
            section: "section",
            sectionName: "sec",
            updateHash: false
        });
    });
</script>