<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('') }}/slick/carousel.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ url('') }}/css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Karla:wght@600&family=PT+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@600&display=swap" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="./slick/slick.css" /> -->
    <link rel="stylesheet" type="text/css" href="{{ url('') }}/slick/slick-theme.css" />
</head>
<body>
    <section class="hero-section d-block">
        <img class="hero-bg" src="{{ url('') }}/img/nature3.jpg" alt="">
        <span class="side-menu-icon" style="font-size:30px;cursor:pointer" onclick="openNav()">
            <i class="fas fa-stream"></i>
        </span>
        <div class="hero-header">
            <p>contact us</p>
            <h1>alasari tea</h1>
        </div>
    </section>
    <div id="contact-us-container">

    </div>

    <link rel="stylesheet" href="{{ url('') }}/css/contact-us.css">

    <script>
         $("#contact-us-container").load("{{ asset('/contact-us-section') }}");
    </script>

    <section class="cd-section contact-us-sec align-items-center text-center row">
    <div class="position-relative text-center col-md-12 title mx-auto my-4 my-md-5">
        <h1>contact us</h1>
        <p>Lets our team handle your requirments</p>
    </div>

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">

        {{ Session::get('success') }}

    </div>
    @endif
    <br>
    <form method="POST" action="{{ url('store') }}" class="row col-md-12 justify-content-center mb-2 mb-md-5 mx-auto px-5">
       <!--  <input name="_token" value="{{ csrf_token() }}"> -->
       @csrf
       <br>
        <input type="text" class="col-lg-3 mb-3 mr-lg-3 mr-0" name="name" placeholder="YOUR NAME">

        <br>
        @error('name')
        <small class="form-text text-danger">{{ $message }}</small>
        @enderror

        <br>
        <input type="text" class="col-lg-3 mb-3 mr-lg-3 mr-0" name="phone" placeholder="YOUR PHONE">
        <br>
        @error('phone')
        <small class="form-text text-danger">{{ $message }}</small>
        @enderror
        <br>
        <input type="text" class="col-lg-3 mb-3 mr-lg-3 mr-0" name="mail" placeholder="YOUR MAIL">
        <br>
        @error('mail')
        <small class="form-text text-danger">{{ $message }}</small>
        @enderror
        <button class="col-lg-2 mb-3">send</button>
    </form>
    <div class="address col-md-12 mb-md-5 mb-3 px-5">
        <p>Plot 7/10, sixth industrial zone- 6th of october city</p>
        <p>Plot 7/10, sixth industrial zone- 6th of october</p>
        <p>02/ 37118324 02/ 38737007</p>
        <p>INFO@ALASARITEA.COM</p>
    </div>

    <div class="social-icons col-md-12">
        <i class="fab fa-facebook-square"></i>
        <i class="fab fa-instagram"></i>
        <i class="fab fa-youtube"></i>
        <i class="fab fa-linkedin"></i>
        <i class="fab fa-twitter"></i>
        <i class="fab fa-whatsapp m-0"></i>
    </div>
</section>


    <section id="footer" class="sec cd-section footer flex-column py-md-5 py-0">

        <div class="row px-3 px-lg-5">

            <div class="col-lg-9 row py-4 justify-content-between m-0 justify-content-lg-start">
                <div class="col-lg-3 col-4">
                    <h3>helpful information</h3>
                    <p>black tea</p>
                    <p>green tea</p>
                    <p>recieps</p>
                    <p>contact us</p>
                    <p>our story</p>
                    <p>certificates</p>
                </div>
                <div class="col-lg-3 col-4 offset-md-2">
                    <h3>about al asari</h3>
                    <p>news</p>
                    <p>sustinability</p>
                    <p>our products</p>
                    <p>quality & standards</p>
                    <p>tea journey</p>
                </div>
            </div>

            <div class="col-lg-3 py-4 text-center">
                <img class="mb-5 mx-auto d-none d-lg-block" src="https://via.placeholder.com/200x200" alt="">
                <div class="row justify-content-center">
                    <p class="certificates-p">certificates</p>
                    <div class="row justify-content-center">
                        <img class="col-lg-3 col-2" src="{{ url('') }}/img/iso1.png" alt="">
                        <img class="col-lg-3 col-2" src="{{ url('') }}/img/iso2.jpg" alt="">
                        <img class="col-lg-3 col-2" src="{{ url('') }}/img/iso3.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
        <p class="text-center mt-4 powered">powered by wein digital</p>
    </section>

</body>