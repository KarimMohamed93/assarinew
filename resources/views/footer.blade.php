    <section id="footer" class="sec cd-section footer flex-column py-md-5 py-0">

        <div class="row px-3 px-lg-5">

            <div class="col-lg-9 row py-4 justify-content-between m-0 justify-content-lg-start">
                <div class="col-lg-3 col-4">
                    <h3>helpful information</h3>
                    <p>black tea</p>
                    <p>green tea</p>
                    <p>recieps</p>
                    <p>contact us</p>
                    <p>our story</p>
                    <p>certificates</p>
                </div>
                <div class="col-lg-3 col-4 offset-md-2">
                    <h3>about al asari</h3>
                    <p>news</p>
                    <p>sustinability</p>
                    <p>our products</p>
                    <p>quality & standards</p>
                    <p>tea journey</p>
                </div>
            </div>

            <div class="col-lg-3 py-4 text-center">
                <img class="mb-5 mx-auto d-none d-lg-block" src="https://via.placeholder.com/200x200" alt="">
                <div class="row justify-content-center">
                    <p class="certificates-p">certificates</p>
                    <div class="row justify-content-center">
                        <img class="col-lg-3 col-2" src="{{ url('') }}/img/iso1.png" alt="">
                        <img class="col-lg-3 col-2" src="{{ url('') }}/img/iso2.jpg" alt="">
                        <img class="col-lg-3 col-2" src="{{ url('') }}/img/iso3.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
        <p class="text-center mt-4 powered">powered by wein digital</p>
    </section>