<section class="hero-section d-block">
    <img class="hero-bg" src="{{ url('') }}/img/nature3.jpg" alt="">
    <span class="side-menu-icon" style="font-size:30px;cursor:pointer" onclick="openNav()">
        <i class="fas fa-stream"></i>
    </span>
    <div class="hero-header">
        <p>contact us</p>
        <h1>alasari tea</h1>
    </div>
</section>
<div id="contact-us-container">

</div>

<link rel="stylesheet" href="{{ url('') }}/css/contact-us.css">

<script>
     $("#contact-us-container").load("{{ asset('/contact-us-section') }}");
</script>