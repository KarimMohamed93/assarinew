<section class="sec hero-section">
    <video class="hero-bg-video" autoplay muted loop id="myVideo">
        <source src="{{ url('') }}/videos/Asary Tea.mp4" type="video/mp4">
        Your browser does not support HTML5 video.
    </video>
    <span class="side-menu-icon" style="font-size:30px;cursor:pointer" onclick="openNav()">
        <i class="fas fa-stream"></i>
    </span>
</section>

<section class="sec d-flex flex-column">
    <div class="marqueee">
        <div class="slide">
            <h1 class="marquee-text">Who We Are</h1>
        </div>
        <div class="slide">
            <h1 class="marquee-text">Who We Are</h1>
        </div>
    </div>

    <section
        class="cd-section second-section row justify-content-center align-items-center text-center position-relative pb-5">
        <div class="col-md-12 pb-5">
            <h3 class="col-md-12">meet the king of natural</h3>
            <p class="col-11 col-md-8 mx-auto">100% Natural. 100% automatic production. <span> Since 1998 for
                    forty-five
                    years we
                    distinguished
                    ourselves
                    selecting of the
                    finest teas that are imported from best farms around the world</span>
            </p>
        </div>

        <button class="col-md-2 col-3 offset-md-9">learn more</button>
    </section>
</section>

<section class="sec cd-section third-section flex-column position-relative">
    <div class="header">
        <h1 class="d-flex justify-content-center align-items-center text-center">al asari black</h1>
    </div>
    <div>
        <div class="col-md-6 interactive_3d" id="asari-black-img-3d">
            <!-- 3d goes here -->
            <img src="{{ url('') }}/images/frame_1.png">
        </div>
        <div dir="rtl" class="three-marquees left">
            <div class="slide">
                <h1 class="marquee-text">al asari black</h1>
            </div>
            <div class="slide">
                <h1 class="marquee-text">al asari black</h1>
            </div>
        </div>
        <div class="three-marquees right">
            <div class="slide">
                <h1 class="marquee-text">al asari black</h1>
            </div>
            <div class="slide">
                <h1 class="marquee-text">al asari black</h1>
            </div>
        </div>
        <div dir="rtl" class="three-marquees left">
            <div class="slide">
                <h1 class="marquee-text">al asari black</h1>
            </div>
            <div class="slide">
                <h1 class="marquee-text">al asari black</h1>
            </div>
        </div>
    </div>

</section>

<section class="sec cd-section fourth-section row">
    <div class="header col-md-12 p-0">
        <h1 class="text-center m-auto position-relative">al asari black<span>Dust</span></h1>
    </div>
    <div class="row kenyan-txt p-0 px-md-5 mt-4 text-center text-md-left">
        <div class="col-md-6">
            <!-- 3d goes here -->
            <img src="{{ url('') }}/images/frame_1.png">
        </div>
        <div class="col-md-4">
            <p>100% black kenyan tea</p>
            <p>Taste the sunshine in every rich cup of al asari Yellow Label tea that is specially
                created for
                Egyptian tea lovers</p>
            <button class="mt-5 float-none float-md-right">learn more</button>
        </div>
    </div>

</section>

<section class="sec cd-section fifth-section row">
    <div class="col-md-12">
        <h3>all you need to</h3>
        <p>Discover The Taste Of The World</p>
    </div>
    <div class="col-md-12">
        <div class="col-md-12">
            <h1 class="square">10000 square <span class="yellow-border">meters</span>
            </h1>
        </div>
        <div class="col-md-12">
            <h2 class="square">10000 <span class="yellow-border">square </span>meters</h2>
        </div>
        <div class="col-md-12">
            <h1 class="square">10000 square <span class="yellow-border">meters</span>
            </h1>
        </div>
        <div class="col-md-12">
            <h2 class="square">10000 <span class="yellow-border">square </span>meters</h2>
        </div>

    </div>
</section>

<section class="sec cd-section sixth-section row p-3 p-md-5 text-center text-md-left">
    <div class="col-md-6">
        <h3>Delicious recipes</h3>
        <p class="col-lg-6 p-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore
            et dolore magna aliqua.</p>
        <button class="mt-md-5 mt-3 mb-4 mb-md-0">learn more</button>
    </div>
    <div class="col-12 col-md-6 p-1 row m-auto">
        <div class="col-6 col-md-6 p-1 p-lg-2">
            <img class="mb-2 col-12 p-0" src="{{ url('') }}/img/tea6.jpg" alt="">
            <img class="col-12 p-0" src="{{ url('') }}/img/tea5.jpg" alt="">
        </div>
        <div class="full-h p-1 p-lg-2 col-6 col-md-6 mt-2 mt-md-0">
            <img class="p-0 col-12" src="{{ url('') }}/img/tea2.jpg" alt="">
        </div>
    </div>
</section>

<section class="sec d-flex flex-column">
<div class="partners row flex-column justify-content-center align-items-center mt-5">
    <h3>our partners</h3>
    <p class="col-lg-6 p-0">Discover our partners around the world</p>
</div>

<section class="customer-logos slider d-flex justify-content-center align-items-center">
    <div class="slide"><img src="{{ url('') }}/images/image1.png"></div>
    <div class="slide"><img src="{{ url('') }}/images/image2.png"></div>
    <div class="slide"><img src="{{ url('') }}/images/image3.png"></div>
    <div class="slide"><img src="{{ url('') }}/images/image4.png"></div>
    <div class="slide"><img src="{{ url('') }}/images/image5.png"></div>
    <div class="slide"><img src="{{ url('') }}/images/image6.png"></div>
    <div class="slide"><img src="{{ url('') }}/images/image7.png"></div>
    <div class="slide"><img src="{{ url('') }}/images/image8.png"></div>
</section>
</section>

<section class="sec cd-section contact-us-sec align-items-center text-center row"></section>

<script>
    $(function () {
        $.scrollify({
            section: "section",
            sectionName: "sec",
            updateHash: false
        });
    });
    $(".contact-us-sec").load("{{ asset('/contact-us-section') }}");
    $("#asari-black-img-3d").interactive_3d({
        frames: 38
    });

    $('.marqueee').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        speed: 20000,
        autoplaySpeed: 0,
        cssEase: 'linear'
    });

    $('.left').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        speed: 20000,
        autoplaySpeed: 100,
        cssEase: 'linear',
        rtl: true
    });
    $('.right').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        speed: 20000,
        autoplaySpeed: 100,
        cssEase: 'linear',
        // slidesToScroll: -1,
        // vertical: true
    });

    $('.customer-logos').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        prevArrow: '<i class="fas fa-chevron-left prev"></i>',
        nextArrow: '<i class="fas fa-chevron-right next"></i>',
        arrows: true,
        cssEase: 'ease',
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
    });
</script>