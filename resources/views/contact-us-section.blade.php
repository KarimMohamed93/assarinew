<section class="cd-section contact-us-sec align-items-center text-center row">
    <div class="position-relative text-center col-md-12 title mx-auto my-4 my-md-5">
        <h1>contact us</h1>
        <p>Lets our team handle your requirments</p>
    </div>

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">

        {{ Session::get('success') }}

    </div>
    @endif
    <br>
    <form method="POST" action="{{ url('store') }}" class="row col-md-12 justify-content-center mb-2 mb-md-5 mx-auto px-5">
       <!--  <input name="_token" value="{{ csrf_token() }}"> -->
       @csrf

        <input type="text" class="col-lg-3 mb-3 mr-lg-3 mr-0" name="name" placeholder="YOUR NAME">


        @error('name')
        <small class="form-text text-danger">{{ $message }}</small>
        @enderror
        <br>
        <input type="text" class="col-lg-3 mb-3 mr-lg-3 mr-0" name="phone" placeholder="YOUR PHONE">
        
        @error('phone')
        <small class="form-text text-danger">{{ $message }}</small>
        @enderror
        <br>
        <input type="text" class="col-lg-3 mb-3 mr-lg-3 mr-0" name="mail" placeholder="YOUR MAIL">
        
        @error('mail')
        <small class="form-text text-danger">{{ $message }}</small>
        @enderror
        <br>
        <button class="col-lg-2 mb-3">send</button>
    </form>
    <div class="address col-md-12 mb-md-5 mb-3 px-5">
        <p>Plot 7/10, sixth industrial zone- 6th of october city</p>
        <p>Plot 7/10, sixth industrial zone- 6th of october</p>
        <p>02/ 37118324 02/ 38737007</p>
        <p>INFO@ALASARITEA.COM</p>
    </div>

    <div class="social-icons col-md-12">
        <i class="fab fa-facebook-square"></i>
        <i class="fab fa-instagram"></i>
        <i class="fab fa-youtube"></i>
        <i class="fab fa-linkedin"></i>
        <i class="fab fa-twitter"></i>
        <i class="fab fa-whatsapp m-0"></i>
    </div>
</section>

<link rel="stylesheet" href="{{ url('') }}/css/contact-us.css">