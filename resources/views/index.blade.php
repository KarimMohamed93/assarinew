<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('') }}/slick/carousel.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ url('') }}/css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Karla:wght@600&family=PT+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@600&display=swap" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="./slick/slick.css" /> -->
    <link rel="stylesheet" type="text/css" href="{{ url('') }}/slick/slick-theme.css" />
</head>

<body>
    <div class="container-fluid scroll-container">
        <div id="page-container"></div>
        <div id="footer-container">
        </div>
    </div>

    <div id="mySidenav" class="sidenav">
        <ul class="nav flex-column">
            <li><a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a></li>
            <li><a href="#home" onclick="loadPage('#home')">Home</a></li>
            <li>
                <a>Brands <i class="fas fa-chevron-up arrow"></i></a>
                <ul class="sub-menu">
                    <li><a href="#brands" onclick="loadPage('#brands')">alasari tea</a></li>
                    <li><a href="#red-brand" onclick="loadPage('#red-brand')">almawazeen tea</a></li>
                    <li><a href="#">demo cafe</a></li>
                </ul>
            </li>
            <li><a href="#">Profile</a></li>
            <li><a href="#our-story" onclick="loadPage('#our-story')">Our story</a></li>
            <li><a href="#contact-us" onclick="loadPage('#contact-us')">Contact Us</a></li>
            <li><a href="#blog" onclick="loadPage('#blog')">Blog</a></li>
        </ul>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/slick/slick.js"></script>
    <script type="text/javascript" src="{{ url('') }}/js/jquery.interactive_3d.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/scrollify/1.0.21/jquery.scrollify.min.js"
        integrity="sha512-UyX8JsMsNRW1cYl8BoxpcamonpwU2y7mSTsN0Z52plp7oKo1u92Xqfpv6lOlTyH3yiMXK+gU1jw0DVCsPTfKew=="
        crossorigin="anonymous"></script>

    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "100%";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }

        function loadPage(page) {
            switch (page) {
                case "#home":
                    $("#page-container").load("{{ asset('/home') }}");
                    break;
                case "#brands":
                    $("#page-container").load("{{ asset('/brands') }}");
                    break;
                case "#contact-us":
                    $("#page-container").load("{{ asset('/contact-us') }}");
                    break;
                case "#red-brand":
                    $("#page-container").load("{{ asset('/red-brand') }}");
                    break;
                case "#our-story":
                    $("#page-container").load("{{ asset('/our-story') }}");
                    break;
                case "#blog":
                    $("#page-container").load("{{ asset('/blog') }}");
                    break;
                default:
                    $("#page-container").load("{{ asset('/home') }}");
                    break;
            }
            closeNav();
        }

        $(document).ready(function () {
       
            var page = document.location.hash;
            loadPage(page);
            window.addEventListener('hashchange', () => {
                page = document.location.hash;
                loadPage(page);
            });

            $("#footer-container").load("{{ asset('/footer') }}");

            $('.nav li > .sub-menu').parent().click(function () {
                $(".arrow").toggleClass('toggle-arrow');
                var submenu = $(this).children('.sub-menu');
                if ($(submenu).is(':hidden')) {
                    $(submenu).slideDown(200);
                } else {
                    $(submenu).slideUp(200);
                }
            });
        });

        // $(function () {
        //         $.scrollify({
        //             section: "section",
        //             sectionName:"sec"
        //         });
        //     });
    </script>

</body>

</html>