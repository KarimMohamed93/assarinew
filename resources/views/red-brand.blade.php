<section class="hero-section">
    <img class="hero-bg" src="{{ url('') }}/img/nature2.jpg" alt="">
    <span class="side-menu-icon" style="font-size:30px;cursor:pointer" onclick="openNav()">
        <i class="fas fa-stream"></i>
    </span>
</section>

<section
    class="cd-section second-section row justify-content-center align-items-center text-center position-relative pb-5 px-lg-5 px-2">
    <div class="col-md-10 pb-5">
        <h3 class="col-12 text-center">meet the king of natural</h3>
        <p class="col-12 text-center text-lg-left">100% Natural. 100% automatic production. Since 1998 for
            forty-five years we distinguished ourselves selecting of the
            finest teas that are imported from best farms around the world forty-five years we distinguished
            ourselves selecting of the finest teas that are imported from best farms around the world
        </p>
    </div>
</section>

<section class="colors-sec red-sec row justify-content-center pb-5 px-lg-5 px-2">
    <h1 class="d-block d-lg-none">red</h1>
    <div class="col-12 col-lg-5 position-relative">
        <h1 class="d-lg-block d-none left-header">red</h1>
        <div class="text-sec">
            <p class="col-12 text-center text-lg-left">100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world
            </p>
            <button class="see-more">see more</button>
        </div>
    </div>
    <div class="col-12 col-lg-7 red-bg circle-bg-container">
        <img src="{{ url('') }}/images/frame_1.png" alt="">
    </div>
</section>

<section class="colors-sec red-sec row justify-content-center pb-5 px-lg-5 px-2 flex-row-reverse">
    <h1 class="d-block d-lg-none">red</h1>
    <div class="col-12 col-lg-5 position-relative">
        <h1 class="d-lg-block d-none right-header">red</h1>
        <div class="text-sec">
            <p class="col-12 text-center text-lg-left">100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world
            </p>
            <button class="see-more">see more</button>
        </div>
    </div>
    <div class="col-12 col-lg-7 red-bg circle-bg-container">
        <img src="{{ url('') }}/images/frame_1.png" alt="">
    </div>
</section>
<!-- <div class="products row flex-column justify-content-center align-items-center mt-5">
    <h3 class="text-center">our products</h3>
    <p class="col-10 col-lg-6 p-0">Discover our partners around the world Discover our partners around the world
        Discover our partners around the world</p>
    <div class="row justify-content-center">
        <button class="col-3 products-btn yellow-btn">Yellow</button>
        <button class="col-3 products-btn red-btn">Red</button>
        <button class="col-3 products-btn green-btn">Green</button>
    </div>
</div>

<section class="customer-logos slider mt-5 d-flex justify-content-center align-items-center">
    <div class="slide"><img src="./images/image1.png"></div>
    <div class="slide"><img src="./images/image2.png"></div>
    <div class="slide"><img src="./images/image3.png"></div>
    <div class="slide"><img src="./images/image4.png"></div>
    <div class="slide"><img src="./images/image5.png"></div>
    <div class="slide"><img src="./images/image6.png"></div>
    <div class="slide"><img src="./images/image7.png"></div>
    <div class="slide"><img src="./images/image8.png"></div>
</section> -->

<link rel="stylesheet" href="{{ url('') }}/css/brands.css">
<script>
    $('.customer-logos').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<i class="fas fa-chevron-left prev"></i>',
        nextArrow: '<i class="fas fa-chevron-right next"></i>',
        arrows: true,
        cssEase: 'ease',
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4,
                arrows: false
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3,
                arrows: false
            }
        }]
    });
</script>