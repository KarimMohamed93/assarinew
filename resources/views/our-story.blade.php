<section class="sec hero-section d-block position-relative">
    <img class="hero-bg" src="{{ url('') }}/img/nature2.jpg" alt="">
    <span class="side-menu-icon" style="font-size:30px;cursor:pointer" onclick="openNav()">
        <i class="fas fa-stream"></i>
    </span>
    <div class="hero-header">
        <p>our story</p>
        <h1>alasari tea</h1>
    </div>
</section>

<section class="sec d-flex flex-column">

<div class="marquee">
    <div class="slide">
        <h1 class="marquee-text">our history</h1>
    </div>
    <div class="slide">
        <h1 class="marquee-text">our history</h1>
    </div>
</div>

<div class="year-sec left-img row justify-content-center position-relative mt-5">
    <h1>1973</h1>
    <img class="col-12 col-lg-5 d-flex justify-content-center" src="./img/market1.jpg" alt="">
    <div class="col-12 col-lg-5 position-relative">
        <div class="text-sec">
            <p class="col-12 text-center text-lg-left">100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world
                100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world ourselves
                selecting of the finest teas that are imported from best farms around the world

            </p>
        </div>
    </div>
</div>
</section>

<section class="sec year-sec right-img row justify-content-center position-relative mt-5">
    <h1>1998</h1>
    <div class="col-12 col-lg-5 position-relative">
        <div class="text-sec">
            <p class="col-12 text-center text-lg-left">100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world
                100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world ourselves
                selecting of the finest teas that are imported from best farms around the world

            </p>
        </div>
    </div>
    <img class="col-12 col-lg-5 d-flex justify-content-center" src="{{ url('') }}/img/market1.jpg" alt="">
</section>

<section class="sec year-sec left-img row justify-content-center position-relative mt-5">
    <h1>2021</h1>
    <img class="col-12 col-lg-5 d-flex justify-content-center" src="{{ url('') }}/img/market1.jpg" alt="">
    <div class="col-12 col-lg-5 position-relative">
        <div class="text-sec">
            <p class="col-12 text-center text-lg-left">100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world
                100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world ourselves
                selecting of the finest teas that are imported from best farms around the world

            </p>
        </div>
    </div>
</section>

<section class="sec year-sec right-img row justify-content-center position-relative mt-5">
    <h1>2025</h1>
    <div class="col-12 col-lg-5 position-relative">
        <div class="text-sec">
            <p class="col-12 text-center text-lg-left">100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world
                100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world ourselves
                selecting of the finest teas that are imported from best farms around the world

            </p>
        </div>
    </div>
    <img class="col-12 col-lg-5 d-flex justify-content-center" src="{{ url('') }}/img/market1.jpg" alt="">
</section>

<section class="sec row flex-column justify-content-center align-items-center p-5 delicious-sec">
    <h3>Delicious recipes</h3>
    <p class="text-center">100% Natural. 100% automatic production. Since 1998 for
        forty-five years we distinguished ourselves selecting of the
        finest teas that are imported from best farms around the world forty-five years we distinguished
        ourselves selecting of the finest teas that are imported from best farms around the world
        100% Natural. 100% automatic production. Since 1998 for
        forty-five years we distinguished ourselves selecting of the
        finest teas that are imported from best farms around the world forty-five years we distinguished
        ourselves selecting of the finest teas that are imported from best farms around the world ourselves
        selecting of the finest teas that are imported from best farms around the world</p>
</section>

<link rel="stylesheet" href="{{ url('') }}/css/our-story.css">

<script>
    $('.marquee').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        speed: 10000,
        autoplaySpeed: 0,
        cssEase: 'linear'
    });


    $(function () {
        $.scrollify({
            section: "section",
            sectionName: "sec",
            updateHash: false
        });
    });
</script>