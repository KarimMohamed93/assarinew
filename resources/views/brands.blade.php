<section class="sec hero-section">
    <img class="hero-bg" src="{{ url('') }}/img/nature2.jpg" alt="">
    <span class="side-menu-icon" style="font-size:30px;cursor:pointer" onclick="openNav()">
        <i class="fas fa-stream"></i>
    </span>
</section>

<section
    class="sec second-section row justify-content-center align-items-center text-center position-relative pb-5 px-lg-5 px-2">
    <div class="col-md-12 pb-5">
        <h3 class="col-12 text-center">meet the king of natural</h3>
        <p class="col-12 text-center text-lg-left">100% Natural. 100% automatic production. Since 1998 for
            forty-five years we distinguished ourselves selecting of the
            finest teas that are imported from best farms around the world forty-five years we distinguished
            ourselves selecting of the finest teas that are imported from best farms around the world
        </p>
    </div>
</section>

<section class="sec colors-sec yellow-sec row justify-content-center">
    <h1 class="d-block d-lg-none order-0">Yellow</h1>
    <div class="col-12 col-lg-6 position-relative order-2 order-lg-1">
        <h1 class="d-lg-block d-none">Yellow</h1>
        <div class="text-sec">
            <p class="col-12 text-center text-lg-left">100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world
            </p>
            <button type="button" class="see-more" onclick="scrollToDiv('yellow')">see more</button>

        </div>
    </div>
    <div class="col-12 col-lg-6 yellow-bg position-relative order-1 order-lg-2">
        <!-- 3d goes here -->
        <img src="{{ url('') }}/images/frame_1.png">
    </div>
</section>

<section class="sec colors-sec green-sec row justify-content-center">
    <h1 class="d-block d-lg-none">Green</h1>
    <div class="col-12 col-lg-6 green-bg position-relative">
        <!-- 3d goes here -->
        <img src="{{ url('') }}/images/frame_1.png">
    </div>
    <div class="col-12 col-lg-6 position-relative">
        <h1 class="d-lg-block d-none">green</h1>
        <div class="text-sec">
            <p class="col-12 text-center text-lg-left">100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world
            </p>
            <button class="see-more" onclick="scrollToDiv('green')">see more</button>
        </div>
    </div>
</section>

<section class="sec colors-sec red-sec row justify-content-center">
    <h1 class="d-block d-lg-none order-0">red</h1>
    <div class="col-12 col-lg-6 position-relative order-2 order-lg-1">
        <h1 class="d-lg-block d-none">red</h1>
        <div class="text-sec">
            <p class="col-12 text-center text-lg-left">100% Natural. 100% automatic production. Since 1998 for
                forty-five years we distinguished ourselves selecting of the
                finest teas that are imported from best farms around the world forty-five years we distinguished
                ourselves selecting of the finest teas that are imported from best farms around the world
            </p>
            <button class="see-more" onclick="scrollToDiv('red')">see more</button>
        </div>
    </div>
    <div class="col-12 col-lg-6 red-bg position-relative order-1 order-lg-2">
        <!-- 3d goes here -->
        <img src="{{ url('') }}/images/frame_1.png">
    </div>
</section>

<section class="sec d-flex flex-column">

    <div class="sec products row flex-column justify-content-center align-items-center mt-5">
        <h3 class="text-center">our products</h3>
        <p class="col-10 col-lg-6 p-0">Discover our partners around the world Discover our partners around the world
            Discover our partners around the world</p>
        <div class="row justify-content-center">
            <button class="col-3 products-btn yellow-btn">Yellow</button>
            <button class="col-3 products-btn red-btn">Red</button>
            <button class="col-3 products-btn green-btn">Green</button>
        </div>
    </div>
    <section class="sec customer-logos slider mt-5 d-flex justify-content-center align-items-center overflow-hidden">
        <div class="slide">
            <div class="interactive-img-slick" id="asari-black-img-3d">
                <img src="{{ url('') }}/images/frame_1.png">
            </div>
        </div>
        <div class="slide">
            <div class="interactive-img-slick" id="asari-black-img-3d-1">
                <img src="{{ url('') }}/images/frame_5.png">
            </div>
        </div>
        <div class="slide">
            <div class="interactive-img-slick" id="asari-black-img-3d-2">
                <img src="{{ url('') }}/images/frame_10.png">
            </div>
        </div>
    </section>

</section>

<link rel="stylesheet" href="{{ url('') }}/css/brands.css">

<script>
    function scrollToDiv(brand) {
        $('html, body').animate({
            scrollTop: $(".products").offset().top
        }, 'slow');
        $(".products-btn").removeClass("active");
        switch (brand) {
            case "yellow":
                $(".yellow-btn").addClass("active");
                break;
            case "green":
                $(".green-btn").addClass("active");
                break;
            case "red":
                $(".red-btn").addClass("active");
                break;
            default:
                break;
        }
    };

    $("#asari-black-img-3d").interactive_3d({
        frames: 38
    });
    $("#asari-black-img-3d-1").interactive_3d({
        frames: 30
    });
    $("#asari-black-img-3d-2").interactive_3d({
        frames: 28
    });

    $('.customer-logos').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        // centerPadding: '100px',
        prevArrow: '<i class="fas fa-chevron-left prev"></i>',
        nextArrow: '<i class="fas fa-chevron-right next"></i>',
        arrows: true,
        draggable: false,
        cssEase: 'ease',
        swipe: false,
        touchMove: false
    });

    $(function () {
        $.scrollify({
            section: "section",
            sectionName: "sec",
            updateHash: false,
        });
    });
</script>